# Appsynth's Cocoapods Template #

Version 0.1

A cocoapod template for create module library including unit test, ui test and sandbox app for run app using library as standalone app.

### What is this repository for? ###

* To setup new module with testable template both unit and ui test and reduce develop time for boilerplate code.
* To keep consistent module structure for optimizable build time for test and archive.

### How do I get set up? ###

on Cocoapods lib create command, add template url parameter

`$ pod lib create MyModule --template-url="https://bitbucket.org/appsynth/ios-podtemplate"`

### Plan to improve ###

* [ ] Module entry template.
* [ ] Option to initiate with coordinator, firebase authentication
* [ ] Improve build for test with Bazel

### Who do I talk to? ###

* Repo owner or admin

### Note ###

Feel free to contribute and suggest any idea :)
